package com.spars.entities;





import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import java.util.Date;
@Entity
@Table(name = "user")
public class User {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
@Id
	@Column(name="userId")
	private int userId;
	private String status;
	@Column(name="email")
	private String email;
	@Column(name="rollNo")
	private int rollNo;
	
	
	@Column(name="arrayNum")
	private String arrayNum;
	
	@Column(name="arrayAlpha")
	private String arrayAlpha;

	@Override
	public String toString() {
		return String.format("User [userId=%s, status=%s, email=%s, rollNo=%s, arrayNum=%s, arrayAlpha=%s]", userId,
				status, email, rollNo, arrayNum, arrayAlpha);
	}

	public User(int userId, String status, String email, int rollNo, String arrayNum, String arrayAlpha) {
		super();
		this.userId = userId;
		this.status = status;
		this.email = email;
		this.rollNo = rollNo;
		this.arrayNum = arrayNum;
		this.arrayAlpha = arrayAlpha;
	}

	public User() {
		super();
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getRollNo() {
		return rollNo;
	}

	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	public String getArrayNum() {
		return arrayNum;
	}

	public void setArrayNum(String arrayNum) {
		this.arrayNum = arrayNum;
	}

	public String getArrayAlpha() {
		return arrayAlpha;
	}

	public void setArrayAlpha(String arrayAlpha) {
		this.arrayAlpha = arrayAlpha;
	}
	
	
	
	
	

	
	
	


	



  

	

	
	

	
}
