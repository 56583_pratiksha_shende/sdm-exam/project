package com.spars.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spars.daos.Response;
import com.spars.dtos.UserDTO;
import com.spars.entities.User;
import com.spars.services.UserServiceImpl;

@CrossOrigin(origins = "*")
@RestController
public class UserControllerImpl {
	@Autowired
	private UserServiceImpl userService;
	
	
	
	@PostMapping("/user")
	public ResponseEntity<?> addUser(@RequestBody User user) {
		User result = userService.save(user);
		return Response.success(result);
	}
	
	
	
}

