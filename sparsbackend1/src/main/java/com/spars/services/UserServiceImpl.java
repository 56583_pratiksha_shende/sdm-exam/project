package com.spars.services;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spars.daos.UserDao;
import com.spars.dtos.DtoEntityConverter;
import com.spars.dtos.UserDTO;
import com.spars.entities.User;
import com.spars.daos.Response;

@Transactional
@Service
public class UserServiceImpl {
	@Autowired
	private UserDao userDao;
	@Autowired
	private DtoEntityConverter converter;

	
	public User save(User user) {

user.setArrayAlpha(user.getArrayAlpha());
user.setArrayNum(user.getArrayNum());
user.setEmail(user.getEmail());	
user.setRollNo(user.getRollNo());
user.setStatus(user.getStatus());
user.setUserId(user.getUserId());

return userDao.save(user);
	}

	


}

